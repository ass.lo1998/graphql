
document. addEventListener("DOMContentLoaded",()=>{

let logOut= document.querySelector('.buttonLogout')
logOut.addEventListener('click', (event)=>{
  event.preventDefault();

    localStorage.removeItem("jwt")
    window.location.href="index.html"
})

// Get the JWT from local storage : JWT is for "Json Web Token"
var jwt = localStorage.getItem("jwt");
console.log("ceci est le token",jwt);
if (jwt === null) {
  window.location.href = "index.html";
}
 if(!jwt){
  window.location.href = "index.html";
}


const container = document.createElement("div");
const section = document.querySelector(".section");
const graph = document.querySelector(".graph");
const main = document.querySelector("main");

console.log(graph,"session");
container.className = "container"
main.appendChild(container);
container.appendChild(section)
container.appendChild(graph)

async function apiFetch(rq){
    return fetch('https://learn.zone01dakar.sn/api/graphql-engine/v1/graphql', {
        method:"POST",

        headers: {
            Authorization: "Bearer " +jwt, // Ajout du JWT dans l'en-tête Authorization
            "Content-Type": "application/json",
            "Accept": "application/json",
        },

        body:JSON.stringify({
            query:rq,
        })
    })
    .then(response => response.json())
    .catch(error => {
        console.error('Erreur apiFetch() :', error);
    })
}

getUser()

function getUser(){

const rq =`
query {
    user {
        email
        login
        firstName
        lastName
        campus
    }
}
`

const objetUser= apiFetch(rq)



objetUser.then((infoUser)=>{
     
    console.log(infoUser);
    document.querySelector('.infoUser0').textContent = "Votre Gitea: "+ infoUser.data.user[0].login
    document.querySelector('.infoUser1').textContent = "Prenom: " +infoUser.data.user[0].firstName
    document.querySelector('.infoUser2').textContent = "nom: "+infoUser.data.user[0].lastName
    document.querySelector('.infoUser3').textContent = "Email: "+infoUser.data.user[0].email
    document.querySelector('.infoUser4').textContent = "Campus: "+ infoUser.data.user[0].campus 

    document.querySelector('.Welcome').textContent = "Welcome in your Profile "+ infoUser.data.user[0].firstName + infoUser.data.user[0].lastName



})
.catch((error) => {
  console.error("Introspection failed at basic infos:", error);
  window.location.href = "index.html"
});
}


const totalXp = ()=>{
    const rq =`
        query {
            transaction_aggregate(where:{type:{_eq:"xp"}, event:{object:{type:{_eq:"module"}}}}){
                aggregate{
                  sum{
                    amount
                  }
                }
              }
        }
    `

    const tXp = apiFetch(rq)


    tXp.then((totalXpResult)=>{
      // if (!totalXpResult) {
      //   localStorage.removeItem("jwt")
      //   window.location.href="index.html"
      //   return
        
      // }
        console.log("totalXp", totalXpResult.data.transaction_aggregate.aggregate.sum.amount)
      // Convert the amount to a string and store it in totalXsp
let totalXsp = totalXpResult.data.transaction_aggregate.aggregate.sum.amount;

// Take the first three characters of totalXsp
let slicedTotalXsp = (totalXsp/1000).toFixed(0)
        document.querySelector("#totalxp").textContent =slicedTotalXsp + " KB"

        
    })
}

totalXp()

const level = ()=>{
  const rq =`
      query {
        user {
        
          events(where:{event:{registrationId:{_eq:55}}}) {
            level
          }
        
        }
      }
  `

  const tXp = apiFetch(rq)

// console.log(tXp,"level");
  tXp.then((levelResult)=>{
    // console.log(levelResult,"level");
const level = levelResult.data.user[0].events[0].level
      // console.log("le", levelResult.data.user[0].events[0].level)
//     // Convert the amount to a string and store it in totalXsp
// let totalXsp = String(levelResult.data.transaction_aggregate.aggregate.sum.amount);

// // Take the first three characters of totalXsp
// let slicedTotalXsp = totalXsp.slice(0,  3);
      document.querySelector("#level").textContent =level

      
  })
}

level()

const auditRadio = () => {
    const rq = `
      query {
          user {
              totalUp
              totalDown
          }
      }
    `;
    const fetchAudit = apiFetch(rq);
  
    fetchAudit.then((audit) => {
      const totalUpMB = Math.round(audit.data.user[0].totalUp / 10000) / 100 
      console.log("RATIO ", totalUpMB + "MB"); // Log the converted value
  
      const totalDownMB = Math.round(audit.data.user[0].totalDown / 10000) / 100 // Correctly convert to MB and round to  2 decimal places
      console.log("RATIO ", totalDownMB + "MB"); // Log the converted value
      const ratio =(totalUpMB/totalDownMB).toFixed(1)

      console.log(ratio);


      const ratio2 = document.querySelector('.ratio')
      ratio2.textContent=  "Ratio: " + ratio
      const Up = document.querySelector(".createBarChartUp")
      const Down =  document.querySelector(".createBarChartDown")

      Up.textContent ="Done: " + totalUpMB + " MB"
      Down.textContent ="Received: " + totalDownMB + " MB"
     

    });
  };
  
  auditRadio();

  

  function totalAudits() {
    const rq = `
        query {
            user {
              validAudits: audits_aggregate(where:{grade:{_gte:1}}){
                aggregate{
                  count
                }
              }
            
            
            invalidAudits: audits_aggregate(where:{grade:{_lt:1}}){
              aggregate{
                count
              }
            }
            }
        }
    `;
    const fetchAudit = apiFetch(rq);

    fetchAudit.then((audit) => {
      let validAudit = audit.data.user[0].validAudits.aggregate.count;
      let invalidAudit = audit.data.user[0].invalidAudits.aggregate.count;
      console.log(validAudit,"val");
  
      // Calculer le total des audits
      let totalAudits = validAudit + invalidAudit;
  
      // Calculer les pourcentages
      let validPercentage = (validAudit / totalAudits) * 100;
      let invalidPercentage = (invalidAudit / totalAudits) * 100;
  
      // Créer le SVG
      let svgWidth = 400;
      let svgHeight = 400;
      let radius = Math.min(svgWidth, svgHeight) / 2;
  
      let svg = `<svg width="${svgWidth}" height="${svgHeight}" xmlns="http://www.w3.org/2000/svg">`;
  
      // Fonction pour dessiner un secteur
      function drawSector(x, y, radius, startAngle, endAngle, color) {
          let startX = x + radius * Math.cos(startAngle);
          let startY = y + radius * Math.sin(startAngle);
          let endX = x + radius * Math.cos(endAngle);
          let endY = y + radius * Math.sin(endAngle);
          let largeArcFlag = endAngle - startAngle <= Math.PI ? 0 : 1;
  
          return `<path d="M${x} ${y} L${startX} ${startY} A${radius} ${radius} 0 ${largeArcFlag} 1 ${endX} ${endY} Z" fill="${color}" />`;
      }
  
      // Dessiner les secteurs
      let validSector = drawSector(svgWidth / 2, svgHeight / 2, radius, 0, (validPercentage / 100) * Math.PI * 2, 'green');
      console.log(validSector,"valid sector");
      let invalidSector = drawSector(svgWidth / 2, svgHeight / 2, radius, (validPercentage / 100) * Math.PI * 2, Math.PI * 2, 'red');
  
      svg += validSector + invalidSector;
      
      // Ajouter des légendes
      // svg += `<text x="${svgWidth / 2}" y="${svgHeight / 2 + 50}" text-anchor="middle" alignment-baseline="middle" font-size="20" fill="orange">Total Audit PASS = ${validAudit}</text>`;
      // svg += `<text x="${svgWidth / 2 + 90}" y="${svgHeight / 2 - 80}" text-anchor="middle" alignment-baseline="middle" font-size="20" fill="white">Total Audit FAIL = ${invalidAudit}</text>`;
  
      svg += `</svg>`;
  

      let circleDiv = document.querySelector(".graphLeft")
      // Insérer le SVG dans votre document
      circleDiv.innerHTML = svg;
      let legend =document.createElement("div");
      legend.classList.add("legend");
      let legendGreen =document.createElement("div");
      legendGreen.classList.add("legendGreen");
      legendGreen.innerHTML=`Audit PASS =${validAudit}`
      let legendRed =document.createElement("div");
      legendRed.innerHTML=`Audit FAIL = ${invalidAudit}`

      legendRed.classList.add("legendRed");
      legend.appendChild(legendGreen);
      legend.appendChild(legendRed);
      circleDiv.appendChild(legend);
  });
  
     
   
}


// let graphLeft =document.querySelector(".graphLeft")
// let legend =document.createElement("div");
// legend.classList.add("legend");
// console.log(legend,"legne");
// graphLeft.appendChild(legend);
totalAudits();



getXPByProject()


function getXPByProject() {
  fetch("https://learn.zone01dakar.sn/api/graphql-engine/v1/graphql", {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
      // Set Basic authentication headers with base64 encoding
      Authorization: `Bearer ${jwt}`,
    },
    body: JSON.stringify({
      query: `query {
        transaction(where: {type: {_eq: "xp"}, object: {type: {_eq: "project"}}}) {
          type
          amount
          object {
            type
            name
          }
        }
      }`,
    }),
  })
    .then((response) => response.json())
    .then((data) => {
      console.log(data, "successsss");
      //TODO: handle display XP by project
      BarPlotXPByProject(data.data.transaction);
    })
    // .catch((error) => {
    //   console.error("Introspection failed at basic infos:", error);
    //    window.location.href = "index.html"
    // });
}


function BarPlotXPByProject(data) {
  var parentWidth = document.getElementById("svg_xp_curve").clientWidth;
  var svgWidth = parentWidth;
  console.log(svgWidth);
  var parentHeight = document.getElementById("svg_xp_curve").clientHeight;
  var svgHeight = parentHeight;
  // Créer le conteneur SVG
  var xpbarplot = document.getElementById("svg_xp_curve");
  const svg = document.createElementNS("http://www.w3.org/2000/svg", "svg");
  svg.setAttribute("width", 1322); // Modifier la largeur selon vos besoins
  svg.setAttribute("height", svgHeight); // Modifier la hauteur selon vos besoins

console.log(svgWidth);

  xpbarplot.appendChild(svg); // Ajouter le SVG au corps du document (vous pouvez choisir un autre conteneur)
  const xpTitle = document.createElementNS("http://www.w3.org/2000/svg", "text")
  xpTitle.setAttribute("x",1322 /1.4)
  xpTitle.setAttribute("y",svgHeight/15)
  xpTitle.style.fontWeight = "bold"
  xpTitle.style.fontSize = "20px"
  xpTitle.style.textShadow = "3px 3px 3px black";
  xpTitle.textContent= "XP earned by Project"
  xpTitle.setAttribute("fill","white")
  svg.appendChild(xpTitle)
  
  var maxAmount = findMaxAmount(data);
  
  addXP(svg, maxAmount, maxAmount / 2, 1322, svgHeight);
  // Ajouter l'axe des ordonnées
  const line = document.createElementNS("http://www.w3.org/2000/svg", "line");
  line.setAttribute("x1", 50 );
  line.setAttribute("x2", 50 );
  line.setAttribute("y1", svgHeight);
  line.setAttribute("y2", 0);
  line.setAttribute("style", "stroke:red;stroke-width:3px");
  svg.appendChild(line);
  // Paramètres du graphique
  const barWidth = 30;
  const barPadding = 20;
  const maxHeight = svgHeight;

  // Position initiale
  let xPosition = 1322 - 1250;

  // Parcourir les données et créer des barres
  data.forEach((entry) => {
    // Créer une barre pour chaque projet
    const rect = document.createElementNS("http://www.w3.org/2000/svg", "rect");
    rect.setAttribute("width", barWidth);
    rect.setAttribute("height", (entry.amount / maxHeight) * maxHeight); // Échelle des hauteurs
    rect.setAttribute("x", xPosition);
    rect.setAttribute("y", maxHeight - entry.amount / 300);
    rect.setAttribute("fill", "blue"); // Couleur de la barre (vous pouvez changer cela)

    // Ajouter des événements pour afficher les informations au survol de la souris
    rect.addEventListener("mouseover", () => {
      showTooltip(entry.object.name, entry.amount);
    });

    

    rect.addEventListener("mouseout", () => {
      hideTooltip();
    });
    // Ajouter la barre au SVG
    svg.appendChild(rect);

    // Mettre à jour la position pour la prochaine barre
    xPosition += barWidth + barPadding;
  });

  // Réinitialiser la position pour l'axe des abscisses
  xPosition = 20;
}

function findMaxAmount(data) {
  // Vérifier si les données sont non vides
  if (data.length === 0) {
    console.error("Les données sont vides.");
    return null;
  }

  // Utiliser reduce pour trouver le montant maximum
  const maxAmount = data.reduce((max, entry) => {
    return entry.amount > max ? entry.amount : max;
  }, data[0].amount);

  return maxAmount;
}

function addXP(svg, max, middle, svgWidth, maxHeight) {
  // Ajouter les valeurs XP à droite de la ligne de l'axe des ordonnées
  const text = document.createElementNS("http://www.w3.org/2000/svg", "text");
  text.textContent = max;
  text.style.fontStyle = "bold";
  text.style.fontSize = "12px";
  text.setAttribute("fill", "white");
  text.setAttribute("x", 0); // Ajustez la position horizontale selon vos besoins
  text.setAttribute("y", ( maxHeight - max / 300 + 6)); // Ajustez la position verticale selon vos besoins
  text.setAttribute("text-anchor", "start"); // Ancrage du texte à gauche

  const mid = document.createElementNS("http://www.w3.org/2000/svg", "text");
  mid.textContent = middle;
  mid.style.fontStyle = "bold";
  mid.setAttribute("fill", "white");
  mid.style.fontSize = "12px";
  mid.setAttribute("x", 0); // Ajustez la position horizontale selon vos besoins
  mid.setAttribute("y", (maxHeight - middle / 300 + 6)); // Ajustez la position verticale selon vos besoins
  mid.setAttribute("text-anchor", "start"); // Ancrage du texte à gauche

  const start = document.createElementNS("http://www.w3.org/2000/svg", "text");
  start.textContent = 0;
  start.style.fontStyle = "bold";
  start.setAttribute("fill", "white");
  start.style.fontSize = "12px";
  start.setAttribute("x", 0); // Ajustez la position horizontale selon vos besoins
  start.setAttribute("y", maxHeight - 0 / 300 + 6); // Ajustez la position verticale selon vos besoins
  start.setAttribute("text-anchor", "start"); // Ancrage du texte à gauche

  // Ajouter le texte au SVG
  svg.appendChild(text);
  svg.appendChild(mid);
  svg.appendChild(start);
}



function showTooltip(projectName, xpAmount) {
  const tooltip = document.getElementById("tooltip");
  tooltip.textContent = projectName +" : " + convertToKB(xpAmount) +" KB";
  tooltip.style.display = "block";
}


function hideTooltip(projectName, xpAmount) {
  const tooltip = document.getElementById("tooltip");
  tooltip.textContent = projectName +" : " + convertToKB(xpAmount) +" KB";
  tooltip.style.display = "none";
}


function convertToKB(value) {
  try {
      // Convertir la valeur en nombre
      let numericValue = parseFloat(value);
      // Diviser la valeur par 1000 pour obtenir le nombre en KB
      let kbValue = numericValue / 1000;
      // Formater le résultat en fonction de la valeur
      let formattedValue = kbValue;
      if (numericValue < 10000) {
          formattedValue = kbValue.toFixed(2); // Pour les valeurs inférieures à 10000, afficher deux décimales
      } else {
          formattedValue = kbValue.toFixed(1); // Pour les autres valeurs, afficher une seule décimale
      }
      // Retourner la valeur en KB
      return formattedValue;
  } catch (error) {
      console.log("La valeur n'est pas valide.");
  }
}


})