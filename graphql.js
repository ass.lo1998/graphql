
document. addEventListener("DOMContentLoaded",()=>{
let form= document.querySelector('form');

form.addEventListener('submit', event => {
    event.preventDefault();

    let inputEmail = document.querySelector('.inputEmail').value;
    let inputPassword = document.querySelector('.inputPassword').value;

    const structBody = {
        inputEmail: inputEmail,
        inputPassword: inputPassword
    };

    fetch('https://learn.zone01dakar.sn/api/auth/signin', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Basic ${btoa(`${inputEmail}:${inputPassword}`)}`
        },
        body: JSON.stringify(structBody)
    })
    .then(response => {
        if (!response.ok) {
            throw new Error('Error during authentication');
        }
        return response.json();
    })
    .then(data => {
        let jwtToken = data; // Assurez-vous que 'token' est le nom correct de la propriété
        console.log(jwtToken);
        // if (!jwtToken) {
        //     console.log("alo");
        //     window.location.href="index.html"
            
        // }
        localStorage.setItem('jwt', jwtToken);
        window.location.href ="graph.html"
    })
    .catch(error => {
        console.error('Authentication failed:', error);
        let errorMessage =  document.querySelector('.errorLogin');
        errorMessage.style.display = 'block';

        setTimeout(()=>{

            errorMessage.style.display = 'none';
        },4000)
    });
});


})
