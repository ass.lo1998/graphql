
# GraphiQl

This project focuses on mastering the GraphQL query language through the creation of a personal profile page. Utilizing the GraphQL endpoint provided by the platform (GraphQL Endpoint), the goal is to query and display personal data on the profile page. A login page is required to access this data, and the project includes the creation of an intuitive user interface with dynamic statistic graphs generated using SVG.

Hosted project link: https://stunning-hamster-c4bc3d.netlify.app/

## Login Page

The login page allows users to securely log in using either their username or email address, along with a password. In case of invalid credentials, appropriate error messages will be displayed to inform the user of the error. Additionally, the login page provides a logout functionality to ensure secure access


## Dashboard
This part consists of two distinct sections: the first contains profile information, while the second is dedicated to a static section comprising two distinct SVG graphs.

#### Profil information
The profile page offers a comprehensive view of user information obtained from the GraphQL endpoint. The elements included in the profile are as follows:

- Basic user identification.
- Amount of XP earned.
- Audit history (successed and failed).
- User levels.


#### Graphic Sections
The statistics section is an essential part of the profile, featuring at least two distinct SVG graphs that reflect various aspects of the user's journey and achievements at school. The included graphs focus on:

- Skills acquired over time.
- Audit statistics, providing a detailed overview of the user's performance and achievements.
## Setup

Follow these steps to set up the project:


```bash
Clone the repository: git clone https://learn.zone01dakar.sn/git/alo/graphql
```

```bash
Navigate to the project directory: cd graphql
```

```bash
Launch with live server
```


## Authors

- [@alo](https://learn.zone01dakar.sn/git/alo)
